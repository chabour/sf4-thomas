<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Manager\CustomEntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article_detail")
     */
    public function show(): Response
    {

        $article = new Article();

        $article->setTitle('mon super titre');
        $article->setContent('tatata ayyay n,,spmfksgf gùdgùgdgdhfhfj jfjjgjfjf kgùkg');
        $article->setAuthor('Thomas');

        return $this->render(
            'article/index.html.twig',
            [
                'article' => $article,
            ]
        );
    }

    /**
     * @return Response
     * @Route("/articles", name="article_list")
     */
    public function list(): Response
    {

        $dm       = $this->getDoctrine();
        $articles = $dm->getRepository(Article::class)->findAll();

        return $this->render(
            'article/list.html.twig',
            [
                'articles' => $articles,
            ]
        );
    }

    /**
     * @return Response
     * @Route("/article/create", name="article_create")
     */
    public function create(Request $request, CustomEntityManager $entityManager): Response
    {

        $article = new Article();
        $form    = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {

            $entityManager->persist($article);

            return $this->redirectToRoute('article_list');
        }

        return $this->render(
            'article/create.html.twig',
            [
                'article_form' => $form->createView(),
            ]
        );
    }

    /**
     * @return Response
     * @Route("/article/details/{id}", name="article_edit")
     */
    public function edit(Article $article, Request $request, CustomEntityManager $dm): Response
    {

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            $dm->flush();

            return $this->redirectToRoute('article_list');
        }

        return $this->render(
            'article/create.html.twig',
            [
                'article_form' => $form->createView(),
            ]
        );
    }


    /**
     * @param Article             $article
     * @param CustomEntityManager $entityManager
     *
     * @return Response
     * @Route("/article/delete/{id}", name="article_delete")
     */
    public function delete(Article $article, CustomEntityManager $entityManager): Response
    {
        if ( !is_null($article) ) {
            $entityManager->remove($article);
        }

        return $this->redirectToRoute('article_list');
    }
}
