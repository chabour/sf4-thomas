<?php
namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;

class CustomEntityManager
{


    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CostumEntityManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function persist(object $entity, $doFlush=true)
    {
         $this->entityManager->persist($entity);

         if($doFlush){
             $this->flush();
         }
    }

    public function flush()
    {
        $this->entityManager->flush();
    }

    public function remove(object $entity)
    {
        $this->entityManager->remove($entity);
        $this->flush();
    }


}